# README.md

When creating the bowling-app I realize the approach I took is not the most effecient.  Originally I wrote a quick effecient application
but was not happy with it. I refactored and created a unique program that showcases my ability to write an Object Oriented program.

# Configuration
Edit ~/.gradle/gradle.proper
```
#Start the gradle daemon
org.gradle.daemon=true
```

# Build
bowling-app uses Maven as the build tool. To Compile navigate via the command prompt to the source directory of the project bowling-app and type:
`mvn package`

# Run
`java -cp target/bowling-app-1.0-SNAPSHOT.jar com.bowling.app.Bowling {text file}`
Sample bowling text files are located at the projects root directory (bowling-app) files include(game.txt, all-zeros.txt and all-faults.txt, perfect-game.txt)

# Test
`mvn test`
