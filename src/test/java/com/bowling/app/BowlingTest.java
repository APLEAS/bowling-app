package com.bowling.app;
import com.bowling.frame.tools.BowlingCard;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

import static org.junit.Assert.assertEquals;

/**
 * Created by APLEAS on 9/11/2016.
 */
public class BowlingTest {
    private Map<String, List<String>> testMap;
    private List<BowlingCard> cardList;
    @Before
    public void initialize() {
        testMap = new HashMap<>();
        cardList = new ArrayList<>();

    }
    /**
     *Test if the array of frames is length 10
     */
    @Test
    public void bowlingGameSizeTest() {
        createMap("game.txt");
        for(Map.Entry<String, List<String>> entry: testMap.entrySet()) {
            BowlingCard card = new BowlingCard(entry.getKey(), entry.getValue());
            cardList.add(card);
        }
        for(BowlingCard c: cardList){
            assertEquals(10, c.getAccumulator().getFrameArray().length);
        }
    }
    /**
     * Checks scores from test file
     */
    @Test
    public void bowlingScoreTest() {
        createMap("game.txt");
        for(Map.Entry<String, List<String>> entry: testMap.entrySet()) {
            BowlingCard card = new BowlingCard(entry.getKey(), entry.getValue());
            cardList.add(card);
        }
        int firstScore = cardList.get(0).getAccumulator().getFrameArray()[9].getTotal();
        int secondScore = cardList.get(1).getAccumulator().getFrameArray()[9].getTotal();

        assertEquals(167, firstScore);
        assertEquals(151, secondScore);



    }

    /**
     * Test if a perfect game total equals 300
     */
    @Test
    public void perfectScoreTest() {
        createMap("perfect-game.txt");
        for(Map.Entry<String, List<String>> entry: testMap.entrySet()) {
            BowlingCard card = new BowlingCard(entry.getKey(), entry.getValue());
            cardList.add(card);
        }
        assertEquals(300, cardList.get(0).getAccumulator().getFrameArray()[9].getTotal());
    }
    /**
     * Checks if the total score is 0 when all of the bowl scores are zero
     */
    @Test
    public void allZeros() {
        createMap("all-zeros.txt");
        for(Map.Entry<String, List<String>> entry: testMap.entrySet()) {
            BowlingCard card = new BowlingCard(entry.getKey(), entry.getValue());
            cardList.add(card);
        }
        for(BowlingCard c: cardList){
            assertEquals(0, c.getAccumulator().getFrameArray()[9].getTotal());
        }

    }

    /**
     * Checks if the total score is 0 when all of the bowl scores are fault
     * */
    @Test
    public void allFaultsTest() {
        createMap("all-faults.txt");
        for(Map.Entry<String, List<String>> entry: testMap.entrySet()) {
            BowlingCard card = new BowlingCard(entry.getKey(), entry.getValue());
            cardList.add(card);
        }
        for(BowlingCard c: cardList){
            assertEquals(0, c.getAccumulator().getFrameArray()[9].getTotal());
        }

    }


    /**
     * Populates testMap with contents from a file.
     * Files are located in src folder
     * @param fileName file containing bowling data
     */
    private void createMap(String fileName){
        File inFile = new File(fileName);
        try (Stream<String> scores = Files.lines(Paths.get(inFile.getAbsolutePath()))) {
            scores.forEach(s -> {
                if (!s.isEmpty()) {
                    String[] pair = s.split(" ");
                    String name = pair[0];
                    String score = pair[1];
                    if (testMap.containsKey(name)) {
                        testMap.get(name).add(score);
                    } else {
                        List<String> scoreList = new ArrayList<>();
                        scoreList.add(score);
                        testMap.put(name, scoreList);
                    }
                }
            });
        } catch (IOException e) {
            System.out.println("cannot find file : " + fileName);
            e.printStackTrace();
        }

    }
}
