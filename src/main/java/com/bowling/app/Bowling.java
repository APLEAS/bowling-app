package com.bowling.app;

import com.bowling.frame.tools.BowlingCard;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Stream;

/**
 * Created by APLEAS on 9/7/2016.
 */
public class Bowling {

    /**
     * Reads a text file (located in src folder) containing bowling score data and prints the score of
     * the associated game.  The contents of the file is read and stored into a HashaMap
     * containing the names of the players as the key and a list of bowl scores as the values.
     * @param args a text file to read
     */
    public static void main(String args[]) {
        Map<String, List<String>> gameMap = new HashMap<>();
        File inFile = null;
        if (0 < args.length) {
            inFile = new File(args[0]);
        }
        if (inFile == null) {
            System.out.println("You must pass a file ");
            System.exit(0);
        } else {
            try (Stream<String> scores = Files.lines(Paths.get(inFile.getAbsolutePath()))) {
                scores.forEach(s -> {
                    if (!s.isEmpty()) {
                        String[] pair = s.split(" ");
                        String name = pair[0];
                        String score = pair[1];
                        if (gameMap.containsKey(name)) {
                            gameMap.get(name).add(score);
                        } else {
                            List<String> scoreList = new ArrayList<>();
                            scoreList.add(score);
                            gameMap.put(name, scoreList);
                        }
                    }
                });
            } catch (IOException e) {
                e.printStackTrace();
            }
            printScore(gameMap);
        }
    }

    /**
     *Prints the contents of the score card
     * @param gameMap map containing player names and list of bowl scores
     */
    private static void printScore(Map<String, List<String>> gameMap) {
        List<BowlingCard> cardList = new ArrayList<>();

        for(Map.Entry<String, List<String>> entry: gameMap.entrySet()) {
            BowlingCard card = new BowlingCard(entry.getKey(), entry.getValue());
            cardList.add(card);
        }

        StringBuilder frame = new StringBuilder();
        frame.append("Frame") ;
        for (int j = 1; j <= 10; j++){
            frame.append("\t\t").append(j);
        }
        System.out.println(frame.toString());
        for(BowlingCard c: cardList)
            System.out.println(c.getScoreCard());


    }






}
