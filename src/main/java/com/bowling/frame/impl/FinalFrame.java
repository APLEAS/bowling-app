package com.bowling.frame.impl;

import com.bowling.frame.Frame;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by APLEAS on 9/10/2016.
 */
public class FinalFrame extends FrameImpl implements Frame {

    private List<Integer> frameList;
    private int score = 0;
    private int total = 0;

    public FinalFrame(){
        frameList = new ArrayList<>();
    }

    @Override
    public boolean isSpare() {
        return false;
    }

    @Override
    public boolean isFinalFrame() {
        return true;
    }

    @Override
    public int giveStrikeBonus() {
        return frameList.get(0) + frameList.get(1);
    }


    @Override
    public int giveSpareBonus() {
        return frameList.get(0);
    }

    @Override
    public int getScore() {
        int total = 0;
        for(Integer frame: frameList){
            if(frame !=null){
                total+=frame;
            }
        }
        return total;
    }
    @Override
    public void setScore(int score) {
        this.score = score;
    }

    @Override
    public void addFrame(String frame){
        frameList.add(stringToInteger(frame));
    }

    @Override
    public void setTotal(int total) {
        this.total = total;
    }

    @Override
    public int getTotal() {
        return total;
    }

    @Override
    public String pinFallToString() {
        StringBuilder s = new StringBuilder();
        for(Integer frame: frameList) {

            if (frame != null) {
                if (frame == 10) {
                    s.append("\tX");
                } else {
                    s.append("\t").append(frame);
                }
            } else {
                s.append("\t0");
            }
        }
        return s.toString();
    }

    @Override
    public String scoreToString() {
        return "\t\t"+total;
    }
}
