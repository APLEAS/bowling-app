package com.bowling.frame.impl;
import com.bowling.frame.Frame;

/**
 * Created by APLEAS on 9/10/2016.
 */
public class Strike extends FrameImpl implements Frame {
    private int score = 10;
    private int total = 0;
    private Integer bonusString;

    @Override
    public boolean isStrike() {
        return true;
    }

    @Override
    public boolean isSpare() {
        return false;
    }

    @Override
    public int giveStrikeBonus() {
        return 10;
    }

    @Override
    public int giveSpareBonus() {
        return 10;
    }

    @Override
    public int getScore() {
        return 10;
    }

    @Override
    public void setTotal(int total) {
        this.total = total;
    }

    @Override
    public int getTotal() {
        return total;
    }

    @Override
    public String pinFallToString() {
        return "\t\tX";
    }

    @Override
    public String scoreToString() {
        return "\t\t"+total;
    }

}
