package com.bowling.frame.impl;

import com.bowling.frame.Frame;


/**
 * Created by APLEAS on 9/10/2016.
 */
public class FrameImpl implements Frame {
    private int score;
    private int total;

    private Integer first, second;
    public FrameImpl(){
    }

    public FrameImpl(String first, String second){
        setFirst(first);
        setSecond(second);
    }

    public int getFirst() {
        return first;
    }


    private void setFirst(String first) {
        this.first = stringToInteger(first);
    }

    public int getSecond() {
        return second;
    }

    private void setSecond(String second) {

        this.second = stringToInteger(second);
    }

    @Override
    public void addFrame(String frame) {

    }

    @Override
    public void setTotal(int total) {
        this.total = total;
    }

    @Override
    public int getTotal() {
        return total;
    }

    @Override
    public boolean isStrike() {
        return false;
    }

    @Override
    public boolean isSpare() {
        return getScore()==10;
    }

    @Override
    public boolean isFinalFrame() {
        return false;
    }

    @Override
    public int giveStrikeBonus() {
        return getScore();
    }

    @Override
    public int giveSpareBonus() {
        if(first !=null) {
            return first;
        }else{return 0;}

    }

    @Override
    public int getScore() {
        int one = 0;
        int two = 0;

        if(first != null)
            one = first;
        if (second!=null)
            two = second;
        return one+two;
    }

    @Override
    public void setScore(int score) {
        this.score = score;

    }
    @Override
    public void setBonus(int bonus){
        setScore(bonus + getScore());
        setTotal(bonus + getTotal());
    }

    @Override
    public String pinFallToString(){
        String one;
        String two;
        if(first == null){
            one = "F";
        }else{
            one = first.toString();
        }
        if(second == null){
            two = "F";
        }else{
            two = second.toString();
        }
        if(isSpare()){
            two = "/";
        }
        return "\t"+one + "\t"+two;
    }
    @Override
    public String scoreToString() {
        return "\t\t"+total;
    }

    @Override
    public Integer stringToInteger(String num) {
        Integer convert = null;
        if(!num.equals("F")){
            convert = Integer.parseInt(num);
            score += convert;
        }
        return convert;
    }


}

