package com.bowling.frame.tools;

import com.bowling.frame.impl.FinalFrame;
import com.bowling.frame.*;
import com.bowling.frame.impl.FrameImpl;
import com.bowling.frame.impl.Strike;
import java.util.List;

/**
 * Created by APLEAS on 9/7/2016.
 */
public class BowlingCard {

    private String name;
    private final FrameAccumulator accumulator;
    private List<String> pinFallList;

    public BowlingCard(String name, List<String> pinFallList){
        accumulator = new FrameAccumulator();
        setName(name);
        setPinFallList(pinFallList);
        createCard(pinFallList);

    }

    /**
     * Converts the list of bowl scores into frames.
     * A FrameAccumulator is used to add and calculate the stats of each
     * frame
     * @param pinFallList list of the players bowl scores
     */
    private void createCard(List<String> pinFallList) {
        int frameCount = 0;
        int indexer = 0;
        Frame finalFrame = new FinalFrame();
        while(indexer<pinFallList.size()){
            String s = pinFallList.get(indexer);
            Frame frame;
            if(frameCount<9){
                if (s.equals("10")) {
                    frame = new Strike();
                    accumulator.addFrame(frame, frameCount);
                    frameCount += 1;
                    indexer += 1;
                } else {
                    frame = new FrameImpl(pinFallList.get(indexer), pinFallList.get(indexer + 1));
                    accumulator.addFrame(frame, frameCount);
                    frameCount += 1;
                    indexer +=2;
                }
            }else{
                finalFrame.addFrame(s);
                indexer +=1;
            }
        }
        accumulator.addFrame(finalFrame, frameCount);
        accumulator.accumulate();
    }

    public FrameAccumulator getAccumulator(){
        return accumulator;
    }

    public String getName() {
        return name;
    }

    private void setName(String name) {
        this.name = name;
    }

    public List<String> getPinFallList() {
        return pinFallList;
    }

    private void setPinFallList(List<String> listScore) {
        this.pinFallList= listScore;
    }

    /**
     * Uses a StringBuilder to create a string of the
     * contents of the scoreCard
     * @return String containing the scoreCard
     */
    public String getScoreCard(){
        StringBuilder scoreCard = new StringBuilder();
        scoreCard.append(name).append("\n");
        scoreCard.append(accumulator.getPinFallS()).append("\n");
        scoreCard.append(accumulator.getScores());
        return scoreCard.toString();
    }
}
