package com.bowling.frame.tools;

import com.bowling.frame.Frame;

/**
 * Created by APLEAS on 9/11/2016.
 */
public class FrameAccumulator {

    private Frame[] frameArray;
    private boolean strike;
    private boolean spare;
    private int score;

    public FrameAccumulator(){
        frameArray = new Frame[10];
    }

    /**
     * Adds a frame to the index of the frameArray
     * @param frame Frame to be added
     * @param index Index to add to frameArray
     */
    public void addFrame(Frame frame, int index) {
        frameArray[index] = frame;
    }

    /**
     * Iterates through frameArray and calculates the total for
     * each frame.  If a frame is a srike or spare bonuses are
     * given out accordingly
     */
    public void accumulate() {
        for (int i = 0; i < frameArray.length; i++) {
            Frame frame = frameArray[i];
            score += frame.getScore();
            if (strike) {
                addStrikeBonus(frame, i);
            }
            if (spare) {
                addSpareBonus(frame, i);
            }
            strike = frame.isStrike();
            spare = frame.isSpare();
            frame.setTotal(score);
        }
    }

    /**
     * If pervious frame was a spare it is given a spare bonus from the current frame
     * @param frame Current Frame
     * @param index Current Index
     */
    private void addSpareBonus(Frame frame, int index) {
        int bonus = frame.giveSpareBonus();
        score += bonus;
        frameArray[index-1].setBonus(bonus);
        spare = false;
    }

    /**
     * If pervious frame was a strike it is given a spare bonus from the current frame
     * @param frame Current Frame
     * @param index Current Index
     */
    private void addStrikeBonus(Frame frame, int index) {
        int bonus;
        if(frame.isStrike() && index <= frameArray.length - 2) {
            bonus = frame.giveStrikeBonus() + frameArray[index + 1].giveSpareBonus();
        }else{
            bonus = frame.giveStrikeBonus();
        }
        score+=bonus;
        frameArray[index-1].setBonus(bonus);
        strike = false;

    }

    public Frame[] getFrameArray() {
        return frameArray;
    }

    public void setFrameArray(Frame[] frameArray) {
        this.frameArray = frameArray;
    }

    /**
     * Iterates through the frameArray and appends scores to
     * a StringBuilder
     * @return String of StringBuilder
     */
    public String getScores(){
        StringBuilder score = new StringBuilder("Score");
        for(Frame f : frameArray){
            score.append(f.scoreToString());
        }
        return score.toString();
    }

    /**
     * Iterates through the frameArray and appends pinFalls to
     * a StringBuilder
     * @return String of StringBuilder
     */
    public String getPinFallS(){
        StringBuilder pinFall = new StringBuilder("Pinfalls");
        for(Frame f: frameArray){
            pinFall.append(f.pinFallToString());
        }
        return pinFall.toString();
    }

}
