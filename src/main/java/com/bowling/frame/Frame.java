package com.bowling.frame;

/**
 * Created by APLEAS on 9/10/2016.
 */
public interface Frame {
    /**
     *Returns true if Frame is a strike otherwise returns false
     * @return boolean
     */
    boolean isStrike();
    /**
     *Returns true if Frame is a spare otherwise returns false
     * @return boolean
     */
    boolean isSpare();
    /**
     *Returns true if Frame is the final frame otherwise returns false
     * @return boolean
     */
    boolean isFinalFrame();
    /**
     *Returns value for a previous frames strike bonus
     * @return int
     */
    int giveStrikeBonus();
    /**
     *Returns value for a previous frames spare bonus
     * @return int
     */
    int giveSpareBonus();
    /**
     *Returns frame score
     * @return int
     */
    int getScore();

    /**
     * Sets the frame score
     * @param score
     */
    void setScore(int score);

    /**
     * Adds a frame to the frame. Used for the final frame.
     * @param frame
     */
    void addFrame(String frame);

    /**
     * Sets the total score up to this frame
     * @param total
     */
    void setTotal(int total);

    /**
     * returns total score up to this frame
     * @return int
     */
    int getTotal();

    /**
     * Sets the bonus if the frame was a strike or a spare
     * @param bonus
     */
    void setBonus(int bonus);

    /**
     *Returns a String of the PinFalls of the frame
     * @return String
     */
    String pinFallToString();
    /**
     *Returns a String of the Score of the frame
     * @return String
     */
    String scoreToString();

    /**
     *RConverts a String to an Integer.  If the String = "F"
     * The Integer is set to null
     * @return String
     */
    Integer stringToInteger(String num);
}
